package example

import fastparse._, NoWhitespace._

object Hello extends App {
  def parser[_: P] = P( "hello" )

  println(fastparse.parse("hello", parser(_)))
  println(fastparse.parse("goodbye", parser(_)))
}

// https://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html