# fastparse

[lihaoyi/fastparse](https://index.scala-lang.org/lihaoyi/fastparse) Writing Fast Parsers Fast in Scala.

# See also
* [johnynek/fastparse-cats](https://index.scala-lang.org/johnynek/fastparse-cats)

# Official documentation
* [*Easy Parsing with Parser Combinators*
  ](https://www.lihaoyi.com/post/EasyParsingwithParserCombinators.html)
  2016-09 Li Haoyi